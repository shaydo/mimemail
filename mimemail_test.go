package mimemail

import (
	"bytes"
	"encoding/base64"
	"regexp"
	"strings"
	"testing"
)

var discreteTests = []struct {
	name        string
	contentType string
	params      []string
	enc         Encoding
	content     string
	expected    string
}{
	{
		name:        "7bit",
		contentType: "text/plain",
		params:      []string{"charset", "utf-8"},
		enc:         SevenBit,
		content:     "this is 7 bit encoded part",
		expected: "Content-Transfer-Encoding: 7bit\r\n" +
			"Content-Type: text/plain; charset=utf-8\r\n\r\n" +
			"this is 7 bit encoded part\r\n",
	},
	{
		name:        "quoted-printable",
		contentType: "text/plain",
		params:      []string{"charset", "utf-8"},
		enc:         QuotedPrintable,
		content:     "we expect 负鼠 to be encoded in QP",
		expected: "Content-Transfer-Encoding: quoted-printable\r\n" +
			"Content-Type: text/plain; charset=utf-8\r\n\r\n" +
			"we expect =E8=B4=9F=E9=BC=A0 to be encoded in QP\r\n",
	},
	{
		name:        "base64",
		contentType: "text/plain",
		params:      []string{"charset", "utf-8"},
		enc:         Base64,
		content:     "we expect 负鼠 to be Base 64 encoded\r\n",
		expected: "Content-Transfer-Encoding: base64\r\n" +
			"Content-Type: text/plain; charset=utf-8\r\n\r\n" +
			"d2UgZXhwZWN0IOi0n+m8oCB0byBiZSBCYXNlIDY0IGVuY29kZWQNCg==\r\n",
	},
}

func TestDiscrete(t *testing.T) {
	for _, tc := range discreteTests {
		t.Run(tc.name, func(t *testing.T) {
			d := NewDiscrete(tc.contentType, tc.params...)
			d.Encoding(tc.enc)
			d.Content([]byte(tc.content))
			b := bytes.NewBuffer(nil)
			d.Write(b)
			if !bytes.Equal(b.Bytes(), []byte(tc.expected)) {
				t.Fatalf("expected\n`%s`\ngot\n`%s`\n", tc.expected, b.String())
			}
		})
	}
}

func Example() {
	// let's assume that contains some image
	var logo []byte
	mixed := NewMultipart("multipart/mixed")
	html := mixed.NewDiscrete("text/html", "charset", "utf-8")
	html.Encoding(QuotedPrintable)
	html.Content([]byte(`<html><body>
		<img src="cid:logo.png">
		<h1>Hi there!</h1></body></html>`))
	img := mixed.NewDiscrete("image.png")
	img.Encoding(Base64)
	img.Header().Add("content-id", "logo.png")
	img.Content(logo)
	msg := NewMessage(mixed)
	msg.Header().Add("Subject", "mimemail is here")
	msg.Header().Add("From", "Sender <sender@example.com>")
	msg.Header().Add("To", "Rcpt <recipient@example.com>")
	w := bytes.NewBuffer(nil)
	// this could be instead
	// w, _ = smtpClient.Data()
	if err := msg.Write(w); err != nil {
		panic(err)
	}
}

func TestCreateEmail(t *testing.T) {
	alt := NewMultipart("multipart/alternative")
	text := NewDiscrete("text/plain", "charset", "utf-8")
	text.Encoding(QuotedPrintable)
	text.Content([]byte("lonely possum sitting and eating"))
	alt.AddPart(text)

	mix := alt.NewMultipart("multipart/related")
	html := mix.NewDiscrete("text/html", "charset", "utf-8")
	html.Content([]byte(`<html>
		<body>
		<img src="cid:possum.png">
		<p>lonely possum sitting and eating</p>
		</body></html>`))
	png := mix.NewDiscrete("image/png")
	png.Header().Add("content-id", "possum.png")
	png.Content([]byte{137, 80, 78, 71, 13, 10, 26, 10, 0, 0, 0, 13, 73, 72, 68, 82, 0, 0, 0, 1, 0, 0, 0, 1, 8, 0, 0, 0, 0, 58, 126, 155, 85, 0, 0, 0, 14, 73, 68, 65, 84, 120, 156, 98, 98, 0, 4, 0, 0, 255, 255, 0, 6, 0, 3, 250, 208, 89, 174, 0, 0, 0, 0, 73, 69, 78, 68, 174, 66, 96, 130})

	msg := NewMessage(alt)
	msg.Header().Add("from", "Fromster <from@example.com>")
	msg.Header().Add("to", "Toaster <to@example.com>")
	msg.Header().Add("subject", "lonely possum")

	b := bytes.NewBuffer(nil)
	msg.Write(b)

	expect := `Content-Type: multipart/alternative; boundary=b1
From: Fromster <from@example.com>
Mime-Version: 1.0
Subject: lonely possum
To: Toaster <to@example.com>

--b1
Content-Transfer-Encoding: quoted-printable
Content-Type: text/plain; charset=utf-8

lonely possum sitting and eating
--b1
Content-Type: multipart/related; boundary=b2

--b2
Content-Transfer-Encoding: base64
Content-Type: text/html; charset=utf-8

PGh0bWw+CgkJPGJvZHk+CgkJPGltZyBzcmM9ImNpZDpwb3NzdW0ucG5nIj4KCQk8cD5sb25lbHkg
cG9zc3VtIHNpdHRpbmcgYW5kIGVhdGluZzwvcD4KCQk8L2JvZHk+PC9odG1sPg==
--b2
Content-Id: possum.png
Content-Transfer-Encoding: base64
Content-Type: image/png

iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAAAAAA6fptVAAAADklEQVR4nGJiAAQAAP//AAYAA/rQ
Wa4AAAAASUVORK5CYII=
--b2--
--b1--
`
	expect = strings.ReplaceAll(expect, "\n", "\r\n")

	msgBytes := b.Bytes()
	sub1 := regexp.MustCompile("alternative; boundary=([A-Z0-9]+)").FindSubmatch(msgBytes)
	if sub1 == nil {
		t.Logf("`%s`", b.String())
		t.Fatal("couldn't find alternative boundary in message")
	}
	msgBytes = bytes.ReplaceAll(msgBytes, sub1[1], []byte("b1"))
	sub2 := regexp.MustCompile("related; boundary=([A-Z0-9]+)").FindSubmatch(msgBytes)
	if sub2 == nil {
		t.Logf("`%s`", b.String())
		t.Fatal("couldn't find related boundary in message")
	}
	msgBytes = bytes.ReplaceAll(msgBytes, sub2[1], []byte("b2"))
	t.Log(string(sub1[1]))
	if !bytes.Equal(msgBytes, []byte(expect)) {
		t.Logf("`%s`", string(msgBytes))
		t.Errorf("message is not as expected")
	}
}

func TestB64Writer(t *testing.T) {
	b := make([]byte, 8192)
	for i := 0; i < 8192; i++ {
		b[i] = 0x30
	}
	out := &bytes.Buffer{}
	wc := newB64Writer(out)
	if _, err := wc.Write(b); err != nil {
		t.Fatal(err)
	}
	if err := wc.Close(); err != nil {
		t.Fatal(err)
	}
	dec := make([]byte, 8192)
	if n, err := base64.StdEncoding.Decode(dec, out.Bytes()); n != 8192 || err != nil {
		t.Fatalf("n=%d, err=%v", n, err)
	}
	if !bytes.Equal(dec, b) {
		t.Errorf("decoded result doesn't match the original")
	}
}

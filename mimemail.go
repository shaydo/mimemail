// Package mimemail implements functionality for creating MIME email messages.
package mimemail

import (
	"crypto/rand"
	"encoding/base32"
	"encoding/base64"
	"fmt"
	"io"
	"mime"
	"mime/quotedprintable"
	"net/textproto"
	"sort"
)

// Encoding is the content transfer encoding method.
type Encoding int

const (
	// SevenBit means that content is not encoded, it has to be valid 7-bit
	// mail-ready data
	SevenBit Encoding = iota
	// QuotedPrintable
	QuotedPrintable
	// Base64
	Base64
)

const (
	contentTransferEncoding = "Content-Transfer-Encoding"
)

// Header is the header of the email message or a MIME entity
type Header struct {
	h map[string][]string
}

// NewHeader returns a new header
func NewHeader() *Header {
	return &Header{
		h: make(map[string][]string),
	}
}

// Add adds a field with the given name and the value to the header
func (h *Header) Add(name, value string) {
	name = textproto.CanonicalMIMEHeaderKey(name)
	h.h[name] = append(h.h[name], value)
}

// Write writes the header to the writer
func (h *Header) Write(w io.Writer) error {
	keys := make([]string, 0, len(h.h))
	for k := range h.h {
		keys = append(keys, k)
	}
	sort.Strings(keys)
	for _, name := range keys {
		for _, value := range h.h[name] {
			if _, err := fmt.Fprintf(w, "%s: %s\r\n", name, value); err != nil {
				return err
			}
		}
	}
	_, err := fmt.Fprint(w, "\r\n")
	return err
}

// Part is a part of the email message
type Part interface {
	// Header returns the header of the email part
	Header() *Header
	// Write writes the part including the header and encoded content to the writer
	Write(io.Writer) error
}

// Message is an email message containing a MIME entiry
type Message struct {
	Part
}

// NewMessage creates a new email message based on the provided part
func NewMessage(part Part) *Message {
	part.Header().Add("mime-version", "1.0")
	return &Message{
		Part: part,
	}
}

// Multipart is a composite entity that can contain multiple parts
type Multipart struct {
	h        *Header
	boundary string
	parts    []Part
}

// NewMultipart returns new Multipart object
func NewMultipart(ct string) *Multipart {
	h := NewHeader()
	boundary := genBoundary()
	ct = mime.FormatMediaType(ct, map[string]string{"boundary": boundary})
	h.Add("content-type", ct)
	return &Multipart{
		h:        h,
		boundary: boundary,
	}
}

// Header returns the header of the Multipart object
func (mp *Multipart) Header() *Header {
	return mp.h
}

// Write writes the Multipart entity including the header and encoded content
// of all the parts to the writer
func (mp *Multipart) Write(w io.Writer) error {
	if err := mp.h.Write(w); err != nil {
		return err
	}
	if len(mp.parts) == 0 {
		return fmt.Errorf("no parts in multipart message")
	}
	for _, p := range mp.parts {
		if _, err := fmt.Fprintf(w, "--%s\r\n", mp.boundary); err != nil {
			return err
		}
		if err := p.Write(w); err != nil {
			return err
		}
	}
	if _, err := fmt.Fprintf(w, "--%s--\r\n", mp.boundary); err != nil {
		return err
	}
	return nil
}

// AddPart adds a new part to the Multipart object
func (mp *Multipart) AddPart(p Part) {
	mp.parts = append(mp.parts, p)
}

// NewMultipart returns a new Multipart object that is already added as a part
// to the parent object
func (mp *Multipart) NewMultipart(ct string) *Multipart {
	nmp := NewMultipart(ct)
	mp.AddPart(nmp)
	return nmp
}

// NewDiscrete returns a new Discrete object that is already added as a part to
// the parent object
func (mp *Multipart) NewDiscrete(ct string, param ...string) *Discrete {
	d := NewDiscrete(ct, param...)
	mp.AddPart(d)
	return d
}

func genBoundary() string {
	var buf [30]byte
	if _, err := rand.Read(buf[:]); err != nil {
		panic(err)
	}
	return base32.StdEncoding.EncodeToString(buf[:])
}

// Discrete is a discret entity that contains a single piece of content of certain type
type Discrete struct {
	h       *Header
	enc     Encoding
	content []byte
}

// NewDiscrete returns a new Discrete object with given content type and
// content type parameters
func NewDiscrete(ct string, param ...string) *Discrete {
	h := NewHeader()
	if len(param) > 0 {
		p := make(map[string]string)
		for i := 0; i < len(param); i += 2 {
			p[param[i]] = param[i+1]
		}
		ct = mime.FormatMediaType(ct, p)
	}
	h.Add("content-type", ct)
	h.h[contentTransferEncoding] = []string{"base64"}
	return &Discrete{
		h:   h,
		enc: Base64,
	}
}

// Header returns the header of the Discrete object
func (d *Discrete) Header() *Header {
	return d.h
}

// Content sets the content of the entity. Content should be given as raw
// bytes, it will be encoded automatically by Write method. If encoding is set
// to SevenBit, the data must be valid 7-bit data as defined in RFC2045.
func (d *Discrete) Content(c []byte) {
	d.content = c
}

// Encoding sets the content transfer encoding. Write will encode content using
// the given method.
func (d *Discrete) Encoding(e Encoding) {
	d.enc = e
	switch e {
	case SevenBit:
		d.h.h[contentTransferEncoding] = []string{"7bit"}
	case Base64:
		d.h.h[contentTransferEncoding] = []string{"base64"}
	case QuotedPrintable:
		d.h.h[contentTransferEncoding] = []string{"quoted-printable"}
	}
}

// Write writes the Discrete entity including the header and content encoded
// according to chosen method to the writer
func (d *Discrete) Write(w io.Writer) error {
	if err := d.h.Write(w); err != nil {
		return err
	}
	var wc io.WriteCloser
	switch d.enc {
	case Base64:
		wc = newB64Writer(w)
	case QuotedPrintable:
		wc = quotedprintable.NewWriter(w)
	default:
		wc = nopCloser{w}
	}
	if _, err := wc.Write(d.content); err != nil {
		return err
	}
	if err := wc.Close(); err != nil {
		return err
	}
	if _, err := fmt.Fprintf(w, "\r\n"); err != nil {
		return err
	}

	return nil
}

// ads no op Close to the writer
type nopCloser struct {
	io.Writer
}

func (nc nopCloser) Close() error {
	return nil
}

// encodes content as base64 and splits into lines of 76 bytes lenght
type b64Writer struct {
	w   io.Writer
	mod int
}

func newB64Writer(w io.Writer) io.WriteCloser {
	bw := &b64Writer{
		w: w,
	}
	return base64.NewEncoder(base64.StdEncoding, bw)
}

func (bw *b64Writer) Write(p []byte) (int, error) {
	var n int
	for bw.mod+len(p) >= 76 {
		till := 76 - bw.mod
		c, err := bw.w.Write(p[:till])
		n += c
		if err != nil {
			return n, err
		}
		bw.mod = 0
		p = p[till:]
		if _, err := fmt.Fprintf(bw.w, "\r\n"); err != nil {
			return n, err
		}
	}
	if len(p) > 0 {
		c, err := bw.w.Write(p)
		n += c
		bw.mod = c
		if err != nil {
			return n, err
		}
	}
	return n, nil
}
